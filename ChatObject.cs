using System;
using System.Collections;
using System.Runtime.Remoting;

//	csc /noconfig /t:library ChatObject.cs
public class ChatObject : MarshalByRefObject {
    private ArrayList users = new ArrayList();
    private ArrayList ChatWindow = new ArrayList();

    public void SendMsg(string msg) {
        if (msg != null) {
            lock (ChatWindow) {
            	ChatWindow.Add(msg);
            }
        }
    }

    public void AddUser(string username) {
        if (username != null) {
            lock (users) {
               users.Add(username);
            }
        }
        Console.WriteLine("Hello, " + username);
    }

    public void RemoveUser(string username) {
        users.Remove(username);
        Console.WriteLine("Bye, " + username);
    }

    public ArrayList Users() {
        return users;
    }

    public ArrayList Chatwindow() {
    	return ChatWindow;
    }
}