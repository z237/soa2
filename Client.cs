using System;
using System.Runtime.Remoting;
using System.Threading;
using System.Collections;

// csc /noconfig /r:ChatObject.dll Client.cs
public class Client {
 	public static void Main() {
    RemotingConfiguration.Configure("Client.exe.config", false);
    ChatObject chat = new ChatObject();

    Console.WriteLine("Input username");
    string username = Console.ReadLine();
    chat.AddUser(username);
    messageCount = 0;

    Thread thread = new Thread(new ThreadStart(() => Listener(chat, username)));
    thread.Start();
	
		//	send message
    while (true) {
    	string message = Console.ReadLine();
    	if (message == "exit") {
    		chat.RemoveUser(username);
    		thread.Abort();
    		return;
    	} else {
				string toSend = username + ": " + message;
        chat.SendMsg(toSend);
    	}
    }
 	}

 	// get message
 	private static int messageCount;
 	private static void Listener(ChatObject chat, string username) {
  	for (; ;) {
      Thread.Sleep(1000);
      ArrayList ChatWindow = chat.Chatwindow();
      for (int i = messageCount; i < ChatWindow.Count; i++) {
      	if (ChatWindow[i].ToString().IndexOf(username) != 0)
      		Console.WriteLine(ChatWindow[i]);
      }
      messageCount = ChatWindow.Count;
  	}
  }
}