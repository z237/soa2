using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Threading;

namespace RemotingChat
{
    public partial class Form1 : Form
    {
        private Thread thread;

        private void btnSend_Click(object sender, EventArgs e)
        {
            string toSend = username + " : ";
            RemObj.SendMsg(toSend + txtSendMsg.Text + "\n");
            txtSendMsg.Text = "";
        }

        private void Form1_FormClosing(object sender, CancelEventArgs e)
        {
            if (isLogin)
            {
                btnLogout_Click(sender, e);
            }
        }
    }
}