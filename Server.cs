using System;
using System.Runtime.Remoting;
using System.Collections;

//	csc /noconfig /r:ChatObject.dll Server.cs
namespace RemotingChat {
    class Server {
        static void Main(string[] args) {
		  		RemotingConfiguration.Configure("Server.exe.config", false);
          Console.WriteLine("Starting Chat Server");
          
          Console.ReadLine();
        }
    }
}
